#include <Windows.h>
#include <stdio.h>

int main()
{
	SYSTEM_INFO sys_info;
	GetSystemInfo(&sys_info);
	int mem_buffer_size = sys_info.dwAllocationGranularity;

	HFILE file;
	HANDLE hMapFile;
	_OFSTRUCT info;
	DWORD fileSize = 0;
	DWORD fileOffset = 0;
	LPSTR buffer;
	unsigned int currentChunk = 0;
	int counter = 0;	

	file = OpenFile(LPCSTR("gibrish.bin"), &info, OF_READ);

	hMapFile = CreateFileMappingA((HANDLE)file,
		NULL,
		PAGE_READONLY,
		0,
		0,
		NULL);

	if (file != HFILE_ERROR)
	{
		fileSize = GetFileSize((HANDLE)file, NULL);
		printf("File successfully opened.\n");

		//ReadFile((HANDLE)file, buffer, BUFFER_SIZE, NULL, NULL);	//Reading the values of the file for the first time.

		while (fileOffset <= (fileSize - mem_buffer_size))
		{
			buffer = (LPSTR)MapViewOfFile(
				hMapFile,
				FILE_MAP_READ,
				0,
				fileOffset,
				mem_buffer_size);

			for (int i = 0; i < mem_buffer_size; i++)
			{
				if (buffer[i] == 0)
				{
					i = mem_buffer_size;
				}
				else
				{
					if (buffer[i] == 'A')
					{
						counter++;
					}
				}
			}	 //Counts A in a chunk of 65536 bytes.

			currentChunk++;	//Moving 1 chunk ahed.
			fileOffset = mem_buffer_size * currentChunk;	//Setting the offset of the file we want to start reading from.

			UnmapViewOfFile(buffer);	//Removing the mapping from the RAM.
		}

		int bytesLeft = fileSize - fileOffset;

		buffer = (LPSTR)MapViewOfFile(
			hMapFile,
			FILE_MAP_READ,
			0,
			fileOffset,
			bytesLeft);

		for (int i = 0; i < mem_buffer_size; i++)
		{
			if (buffer[i] == 0)
			{
				i = mem_buffer_size;
			}
			else
			{
				if (buffer[i] == 'A')
				{
					counter++;
				}
			}
		}	 //Counts A in a chunk of 65536 bytes.

		UnmapViewOfFile(buffer);	//Removing the mapping from the RAM.

		printf("The total number of A's in the file: %d\n", counter);

		if (CloseHandle((HANDLE)file))
		{
			printf("File closed properly.\n");
		}

		if (CloseHandle(hMapFile))
		{
			printf("Map closed properly.\n");
		}
		
	}
	else
	{
		printf("Error opening file.\n");
	}

	system("pause");
	return 0;
}

//SetFilePointer((HANDLE)file, BUFFER_SIZE, NULL, FILE_BEGIN);