#include <Windows.h>
#include <string>
#include <stdio.h>
#include <iostream>

#define FILE_NAME "gibrish.bin"

HANDLE isAvailable;

int main(int argc, char* argv[])
{
	SYSTEM_INFO sys_info;
	GetSystemInfo(&sys_info);
	int mem_buffer_size = sys_info.dwAllocationGranularity;

	if (argc == 1)
	{
		HANDLE hMap;

		PROCESS_INFORMATION pi = { NULL };
		STARTUPINFO si = { NULL };
		HFILE file;
		_OFSTRUCT info;
		int fileSize = 0;
		int counter = 0;
		int chunk = 0;

		isAvailable = CreateMutex(NULL, false, "fileAvailable");

		WaitForSingleObject(isAvailable, INFINITE);
		file = OpenFile((LPCSTR)("gibrish.bin"), &info, OF_READWRITE);

		hMap = CreateFileMapping((HANDLE)file,
			NULL,
			PAGE_READWRITE,
			0,
			0,
			"mappedFile");
		if (file != HFILE_ERROR)
		{
			printf("File successfully opened.\n");
			std::string name = "Ex2.exe arg";

			const char* fileName = name.c_str();

			CreateProcessA(NULL,
				(LPSTR)fileName,
				NULL,
				NULL,
				FALSE,
				0,
				NULL,
				NULL,
				&si,
				&pi);	//Creating a new process, using a parameter passed through argv.)
			
			ReleaseMutex(isAvailable);

			Sleep(50);	//Waiting for the second process to take control over the mutex object.

			WaitForSingleObject(isAvailable, INFINITE);

			CloseHandle(hMap);
			CloseHandle((HANDLE)file);

		}


		system("Pause");
	}
	else
	{
		isAvailable = CreateMutex(NULL, NULL, "fileAvailable");
		WaitForSingleObject(isAvailable, INFINITE);

		HANDLE hMap;
		LPSTR buffer;

		hMap = OpenFileMappingA(FILE_MAP_WRITE,
			false,
			"mappedFile");

		buffer = (LPSTR)MapViewOfFile(
			hMap,
			FILE_MAP_WRITE,
			0,
			0,
			mem_buffer_size);
	
		buffer[0] = 'Z';	//The Homework.

		//	The Wanna-be-creative thing ;)
		buffer[1] = 'I';
		buffer[2] = 'k';
		buffer[3] = 'n';
		buffer[4] = 'k';
		buffer[5] = '2';

		FlushViewOfFile(buffer, mem_buffer_size);
		printf("File updated!\n");
		ReleaseMutex(isAvailable);
	}



	return 0;
}